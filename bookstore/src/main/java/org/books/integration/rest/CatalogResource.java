/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.rest;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import org.books.application.exception.BookNotFoundException;
import org.books.integration.amazon.AmazonCatalog;
import org.books.persistence.entity.Book;

/**
 *
 * @author baechlerm
 */
@Stateless
@Path("books")
public class CatalogResource {
    
    @EJB
    private AmazonCatalog amazonCatalog;
    
    @GET
    @Path("{isbn}")
    @Produces({APPLICATION_JSON})
    public Book findBook(@PathParam("isbn") String isbn){
        Book book = new Book();
        
        // noch schauen wegen Rollback
        // message vom BookNotFoundException
       
        try {
            book = amazonCatalog.itemLookup(isbn);     
        } catch (BookNotFoundException ex) {
            throw new WebApplicationException(ex.getMessage(),
            Response.status(NOT_FOUND).entity(ex.getMessage()).build());
        }
        
        return book;
    }
}
