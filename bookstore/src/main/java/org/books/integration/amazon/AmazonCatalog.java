/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.amazon;

import org.books.integration.amazon.generated.ItemLookup;
import org.books.integration.amazon.generated.ItemLookupRequest;
import org.books.integration.amazon.generated.Items;
import org.books.integration.amazon.generated.Errors;
import org.books.integration.amazon.generated.ItemLookupResponse;
import org.books.integration.amazon.generated.Item;
import org.books.integration.amazon.generated.AWSECommerceServicePortType;
import org.books.integration.amazon.generated.ItemAttributes;
import org.books.integration.amazon.generated.AWSECommerceService;
import org.books.integration.amazon.generated.ItemSearchRequest;
import org.books.integration.amazon.generated.ItemSearchResponse;
import org.books.integration.amazon.generated.ItemSearch;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.books.application.exception.BookNotFoundException;
import org.books.persistence.dto.BookInfo;
import org.books.persistence.entity.Book;
import org.books.persistence.enumeration.BookBinding;

/**
 *
 * @author baechlerf
 */

@Singleton
public class AmazonCatalog {
    
    private final static String endpoint = "http://webservices.amazon.com/AWSECommerceService/AWSECommerceService.wsdl";
    private final static BigDecimal hundert = new BigDecimal(100);
    private final static long minWaitTime = 1100;
    
    private Logger logger = Logger.getLogger(AmazonCatalog.class.getName());
    private AWSECommerceServicePortType repository;
    private int maxAnzahlSeiten = 10; // TODO AA 19.01.2019 konfigurierbar machen
    
    private LocalTime localTime = null;
    
    @PostConstruct 
    public void init() {
        
        AWSECommerceService service = new AWSECommerceService();
        repository = service.getAWSECommerceServicePort();
        //((BindingProvider)repository).getRequestContext().put(ENDPOINT_ADDRESS_PROPERTY, endpoint);
    
    } 
    
 
    public Book itemLookup(String isbn) throws BookNotFoundException {
        
        slowDown();
        
        
        ItemLookup body = new ItemLookup();
        
        ItemLookupRequest request = new ItemLookupRequest();
        // TODO AA 18.01.2017 Konfigurierbar machen?
        request.setSearchIndex("Books");
        request.setIdType("ISBN");
        request.getItemId().add(isbn);
        request.getResponseGroup().add("ItemAttributes");
       
        body.getRequest().add(request);
        
        localTime = LocalTime.now();
        System.out.println("nach dem SlowDown/setzen bei itemLookup. LocalTime; " +localTime.toString());
        
        ItemLookupResponse result = repository.itemLookup(body);
        
        List<Items> itemList = result.getItems();

        checkFehlerMeldung(itemList);

        for (Items items : itemList) {
            for (Item item : items.getItem()) {
                ItemAttributes itemAttributes = item.getItemAttributes();
                Book book = getBook(itemAttributes);
                if (book.isComplete()) {
                    return book;
                } else {
                    logger.log(Level.WARNING, "Buch nicht komplett {0} ", book);
                }
            }
        }
        
        throw new BookNotFoundException();
    }
    
    
    public List<BookInfo> itemSearch(String keywords) throws BookNotFoundException {
        
        slowDown();
        
        List<BookInfo> bookInfo = new ArrayList<>();

        ItemSearchResponse result = search(keywords, 1);
        bookInfo.addAll(getBookInfos(result));
        
        int anzahlSeiten = getAnzahlSeiten(result);
        for (int i = 2; i <= Integer.min(anzahlSeiten, maxAnzahlSeiten); i++) {
            result = search(keywords, i);
            bookInfo.addAll(getBookInfos(result));
        }
        
        return bookInfo;
    }

    private String checkFehlerMeldung(List<Items> items) throws BookNotFoundException {
        for (Items item : items) {
            if (item.getRequest().getErrors() != null) {
                List<Errors.Error> errors = item.getRequest().getErrors().getError();
                for (Errors.Error error : errors) {
                    if ("AWS.InvalidParameterValue".equals(error.getCode())) {
                        throw new BookNotFoundException();
                    } else {
                        throw new RuntimeException(error.getCode() + " -> " + error.getMessage());
                    }
                }
                
            }
        }
        return null;
    }

    private Book getBook(ItemAttributes itemAttributes) {
        Book book = new Book();
        boolean bookComplete = true;
//        String isbn, 
        if (StringUtils.isNotBlank(itemAttributes.getISBN())) {
            book.setIsbn(itemAttributes.getISBN());
        } else {
            log("ISBN");
            bookComplete = false;
        }
//        String title, 
        if (StringUtils.isNotBlank(itemAttributes.getTitle())) {
            book.setTitle(itemAttributes.getTitle());
        } else {
            log("Titel");
            bookComplete = false;
        }
//        String authors, 
        if (itemAttributes.getAuthor() != null && ! itemAttributes.getAuthor().isEmpty()) {
            StringBuilder sb = new StringBuilder();
            String trenner = "";
            for (String author : itemAttributes.getAuthor()) {
                sb.append(trenner).append(author);
                trenner = ", ";
            }
            book.setAuthors(sb.toString());
        } else {
            log("Author");
            bookComplete = false;
        }
//        String publisher, 
        if (StringUtils.isNotBlank(itemAttributes.getPublisher())) {
            book.setPublisher(itemAttributes.getPublisher());
        } else {
            log("Publisher");
            bookComplete = false;
        }
//        Integer publicationYear, 
        if (StringUtils.isNotBlank(itemAttributes.getPublicationDate())) {
            book.setPublicationYear(Integer.parseInt(itemAttributes.getPublicationDate().substring(0, 4)));
        } else {
            log("PublicationDate");
            bookComplete = false;
        }
//        BookBinding binding, 
        if (StringUtils.isNotBlank(itemAttributes.getBinding())) {
            String binding = itemAttributes.getBinding().toUpperCase();
            if (EnumUtils.isValidEnum(BookBinding.class, binding)) {
                book.setBinding(BookBinding.valueOf(binding));
            } else {
                logger.log(Level.INFO, "Neues Binding {0} ", itemAttributes.getBinding());
                book.setBinding(BookBinding.UNKNOWN);
            }
        } else {
            log("Binding");
            bookComplete = false;
        }
//        Integer numberOfPages, 
        if (itemAttributes.getNumberOfPages() != null) {
            book.setNumberOfPages(itemAttributes.getNumberOfPages().intValue());
        } else {
            log("NumberOfPages");
            bookComplete = false;
        }
//        BigDecimal price
        if (itemAttributes.getListPrice() != null && itemAttributes.getListPrice().getAmount() != null) {
            book.setPrice(new BigDecimal(itemAttributes.getListPrice().getAmount()).divide(hundert));
        } else {
            log("ListPrice.Amount");
            bookComplete = false;
        }
        book.setComplete(bookComplete);
        return book;
    }

    private void log(String field) {
        logger.log(Level.WARNING, "Buch nicht g\u00fcltig weil {0} nicht gesetzt ist!", field);
    }

    private ItemSearchResponse search(String keywords, int i) {
        ItemSearchResponse result = null;

        boolean keinResultat = true;
        while(keinResultat) {
            try {
                result = serviceCall(keywords, i);
                keinResultat = false;
            } catch (Exception ex) {
                System.out.println("ex.getCause:"+ex.getClass());
                System.out.println("ex.getCause:"+ex.getCause());
                System.out.println("ex.getLocalizedMessage:"+ex.getLocalizedMessage());
                System.out.println("ex.getMessage:"+ex.getMessage());
//Information:   ex.getLocalizedMessage:Der Server hat HTTP-Statuscode 503 gesendet: Service Unavailable
//Information:   ex.getMessage:Der Server hat HTTP-Statuscode 503 gesendet: Service Unavailable

// FB so wie ich das sehe, machst du ein warten nachdem ein 503 kommt...(Wo sind die Daten vom fehlerhaften Aufruf?) 
// Wollen wir das so lassen, oder immer die minWaitTime warten, weil wenn wir die Kommentare ausschalten, ist dann alles viel schnelle...
                if (ex.getMessage().contains("503")) {
                    try {                
                        Thread.sleep(minWaitTime);
                    } catch (InterruptedException ex1) {
                        Logger.getLogger(AmazonCatalog.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                }
            }
        }
                
        return result;
    }
    
    private ItemSearchResponse serviceCall(String keywords, int i) {
        ItemSearch body = new ItemSearch();
        
        ItemSearchRequest request = new ItemSearchRequest();
        // TODO AA 18.01.2017 Konfigurierbar machen?
        request.setSearchIndex("Books");
        request.setKeywords(keywords);
        request.getResponseGroup().add("ItemAttributes");
        request.setItemPage(new BigInteger(i+""));
        
        body.getRequest().add(request);
        
        localTime = LocalTime.now();
        
        return repository.itemSearch(body);
    }
    
    private List<BookInfo> getBookInfos(ItemSearchResponse result) throws BookNotFoundException {
        List<Items> itemList = result.getItems();
        checkFehlerMeldung(itemList);
        List<BookInfo> books = new ArrayList<>();
        for (Items items : itemList) {
            for (Item item : items.getItem()) {
                ItemAttributes itemAttributes = item.getItemAttributes();
                Book book = getBook(itemAttributes);
                if (book.isComplete()) {
                    books.add(new BookInfo(book.getIsbn(), book.getTitle(), book.getPrice()));
                } else {
                    logger.log(Level.WARNING, "Buch nicht komplett {0} ", book);
                }
            }
        }
        return books;
    }

    private int getAnzahlSeiten(ItemSearchResponse result) {
        if (result.getItems() != null && result.getItems().size() > 0) {
            return result.getItems().get(0).getTotalPages().intValue();
        }
        return 0;
    }
 
    
    private void slowDown() {
       
        LocalTime currentLocalTime = LocalTime.now();
        
        if (localTime == null) {
            localTime = LocalTime.now();
            
            try {
                System.out.println("warten für " + minWaitTime + " ms");
                Thread.sleep(minWaitTime);
            } catch (InterruptedException ex) {
                Logger.getLogger(AmazonCatalog.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            currentLocalTime = LocalTime.now();
        }
        
        long diff = java.time.Duration.between(localTime, currentLocalTime).toMillis();
        System.out.println("different ist: " + diff);
        
        
        if (diff < minWaitTime) {
            
            long restWaitTime = minWaitTime - diff;
            
            try {
                System.out.println("warten für : " + restWaitTime +"ms");
                Thread.sleep(restWaitTime);
            } catch (InterruptedException ex) {
                Logger.getLogger(AmazonCatalog.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
