/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.amazon;

import org.books.integration.amazon.generated.SecurityHelperDto;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import static javax.xml.ws.handler.MessageContext.MESSAGE_OUTBOUND_PROPERTY;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;

/**
 *
 * @author baechlerf
 */
public class AmazonSecurityHelperFB implements SOAPHandler<SOAPMessageContext> {

    private final String ASSOCIATE_TAG = "test0e5d-20";
    private final String ACCESS_KEY = "AKIAIYFLREOYORYNAQTQ";
    private final String SECRET_KEY = "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";
    private final String TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private final String MAC_ALGORITHM = "HmacSHA256";

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        System.out.println("Bin in der handleMessage Methode");

        SOAPMessage message = context.getMessage();
        if ((Boolean) context.get(MESSAGE_OUTBOUND_PROPERTY)) {
            SecurityHelperDto dto = generate("itemLookup");
            
            try {
                
                SOAPHeader header = message.getSOAPHeader();
                
                SOAPBody body = message.getSOAPBody();
                
                
                SOAPElement bodyElement = (SOAPElement)body.getFirstChild();
                SOAPElement contactElement = (SOAPElement)bodyElement.getFirstChild();
                Iterator<SOAPElement> iter = contactElement.getChildElements();
                
                System.out.println("Name: " + iter.next().getValue());
                System.out.println("Phone: " + iter.next().getValue());
                System.out.println("Email: " + iter.next().getValue());
                
                
                
            } catch (SOAPException ex) {
                Logger.getLogger(AmazonSecurityHelperFB.class.getName()).log(Level.SEVERE, null, ex);
            }
            

        } else {
            System.out.println("ist eine Inbound Message");
        }
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close(MessageContext context) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private SecurityHelperDto generate(String methodeName) {

        SecurityHelperDto result = new SecurityHelperDto();
        
        DateFormat dateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);
        String timestamp = dateFormat.format(Calendar.getInstance().getTime());

        Mac mac = null;
        SecretKey key = new SecretKeySpec(SECRET_KEY.getBytes(), MAC_ALGORITHM);
        
        try {
            mac = Mac.getInstance(MAC_ALGORITHM);
            mac.init(key);
             
        } catch (NoSuchAlgorithmException | InvalidKeyException ex) {
            Logger.getLogger(AmazonSecurityHelperFB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        byte[] data = mac.doFinal((methodeName + timestamp).getBytes());
        String signature = DatatypeConverter.printBase64Binary(data);
        
        System.out.println("AssociateTag:   " + ASSOCIATE_TAG);
        System.out.println("AWSAccessKeyId: " + ACCESS_KEY);
        System.out.println("Timestamp:      " + timestamp);
        System.out.println("Signature:      " + signature);
        
        result.setAssociateTag(ASSOCIATE_TAG);
        result.setAwsAccessKeyId(ACCESS_KEY);
        result.setSignatur(signature);
        result.setTimeStamp(timestamp);
        
        return result;
    }

}
