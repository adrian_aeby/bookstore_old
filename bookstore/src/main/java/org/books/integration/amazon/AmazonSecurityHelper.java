/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.amazon;

import org.books.integration.amazon.generated.SecurityHelperDto;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 *
 * @author Adrian
 */
public class AmazonSecurityHelper implements SOAPHandler<SOAPMessageContext> {

    private final String ASSOCIATE_TAG = "test0e5d-20";
    private final String ACCESS_KEY = "AKIAIYFLREOYORYNAQTQ";
    private final String SECRET_KEY = "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";
    private final String TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private final String MAC_ALGORITHM = "HmacSHA256";

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        
        SOAPMessage message = context.getMessage();
        
        
        if (!(Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY)) {
            System.out.println("OUTBOUND...");
            
            try {
                message.writeTo(System.out);
                System.out.println();
            } catch (SOAPException | IOException ex) {
                Logger.getLogger(AmazonSecurityHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("INBOUND...");

            SecurityHelperDto securityHelperDto;

            try {

                SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();
                SOAPBody body = envelope.getBody();

                SOAPElement methodNode = (SOAPElement) body.getFirstChild();

                securityHelperDto = generate(methodNode.getNodeName());

                //SOAPFactory soapFactory = SOAPFactory.newInstance();
                //SOAPElement associateTagNode = soapFactory.createElement("AssociateTag");
                // TODO wird es immer als erstes Element angehängt oder muss mit
                // methodNode.insertBefore(associateTagNode, firstMethodNode);   gearbeitet werden?
                SOAPElement associateTag = methodNode.addChildElement("AssociateTag");
                associateTag.setValue(securityHelperDto.getAssociateTag());

                SOAPHeader header = envelope.getHeader();

                header.addNamespaceDeclaration("aws", "http://security.amazonaws.com/doc/2007-01-01/");
                SOAPElement accessKeyId = header.addChildElement("AWSAccessKeyId", "aws");
                accessKeyId.setValue(securityHelperDto.getAwsAccessKeyId());
                SOAPElement timestamp = header.addChildElement("Timestamp", "aws");
                timestamp.setValue(securityHelperDto.getTimeStamp());
                SOAPElement signature = header.addChildElement("Signature", "aws");
                signature.setValue(securityHelperDto.getSignatur());

                message.writeTo(System.out);
                System.out.println();
            } catch (SOAPException | IOException ex) {
                Logger.getLogger(AmazonSecurityHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return true;
    }

    @Override
    public void close(MessageContext context) {

    }

    private SecurityHelperDto generate(String methodeName) {

        SecurityHelperDto result = new SecurityHelperDto();

        DateFormat dateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);
        String timestamp = dateFormat.format(Calendar.getInstance().getTime());

        Mac mac = null;
        SecretKey key = new SecretKeySpec(SECRET_KEY.getBytes(), MAC_ALGORITHM);

        try {
            mac = Mac.getInstance(MAC_ALGORITHM);
            mac.init(key);

        } catch (NoSuchAlgorithmException | InvalidKeyException ex) {
            Logger.getLogger(AmazonSecurityHelperFB.class.getName()).log(Level.SEVERE, null, ex);
        }

        byte[] data = mac.doFinal((methodeName + timestamp).getBytes());
        String signature = DatatypeConverter.printBase64Binary(data);

        result.setAssociateTag(ASSOCIATE_TAG);
        result.setAwsAccessKeyId(ACCESS_KEY);
        result.setSignatur(signature);
        result.setTimeStamp(timestamp);

        return result;
    }
}
