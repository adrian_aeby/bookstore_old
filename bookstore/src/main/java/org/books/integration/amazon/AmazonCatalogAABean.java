/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.amazon;

import org.books.integration.amazon.generated.AWSECommerceServicePortType;
import org.books.integration.amazon.generated.AWSECommerceService;
import org.books.integration.amazon.generated.ItemSearchRequest;
import org.books.integration.amazon.generated.ItemSearchResponse;
import org.books.integration.amazon.generated.ItemSearch;
import javax.ejb.Stateless;

/**
 *
 * @author Adrian
 */
@Stateless
public class AmazonCatalogAABean implements AmazonCatalogAA{
    public void test() {
        AWSECommerceService service = new AWSECommerceService();
        AWSECommerceServicePortType servicePort = service.getAWSECommerceServicePort();
        ItemSearch body = new ItemSearch();
        body.setAssociateTag("test0e5d-20");
        ItemSearchRequest request = new ItemSearchRequest();
        request.setSearchIndex("Books");
        request.setKeywords("android");
        request.getResponseGroup().add("ItemAttributes");
        body.getRequest().add(request);
        ItemSearchResponse response = servicePort.itemSearch(body);
        System.out.println(response.getItems().get(0).getRequest().getIsValid());
        
    }
}
