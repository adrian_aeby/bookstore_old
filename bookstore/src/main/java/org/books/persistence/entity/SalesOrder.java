/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence.entity;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import org.books.persistence.enumeration.OrderStatus;

/**
 *
 * @author Adrian
 */
@Entity
@Table(name = "SALESORDER")
@NamedQueries({
    @NamedQuery(name = SalesOrder.GET_SALESORDER_BY_ORDERNUMBER, 
            query = "select o from SalesOrder o "
                + "where o.number = :number")
})
@NamedEntityGraph(name = "includeSalesItems",
        attributeNodes = @NamedAttributeNode("items")
)
public class SalesOrder extends BaseEntity {
    
    public final static String GET_SALESORDER_BY_ORDERNUMBER = "SalesOrder.getSalesOrderByOrderNumber";

    @Id
    @GeneratedValue
    private Long number;
    private Date date;
    @Column(columnDefinition = "DECIMAL(7,2)")
    private BigDecimal amount;
    @Enumerated(EnumType.STRING)
    private OrderStatus status; 
    
    @Embedded
    private Address address;
    @Embedded
    private CreditCard creditCard;
    
    @ManyToOne
    private Customer customer;
    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, orphanRemoval = true)
    @JoinColumn(name = "salesOrder_id")
    private List<SalesOrderItem> items;

    public SalesOrder() {
    }

    public SalesOrder(Date date, BigDecimal amount, OrderStatus status, Customer customer, Address address, CreditCard creditCard, List<SalesOrderItem> items) {
        this.date = date;
        this.amount = amount;
        this.status = status;
        this.customer = customer;
        this.address = address;
        this.creditCard = creditCard;
        this.items = items;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @PrePersist
    @PreUpdate
    public void setAmount() {
        BigDecimal amount = new BigDecimal(0);
        for (SalesOrderItem item : getSalesOrderItems()) {
            amount = amount.add(item.getPrice().multiply(new BigDecimal(item.getQuantity())));
        }
        this.amount = amount;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<SalesOrderItem> getSalesOrderItems() {
        if (items == null) {
            items = new ArrayList<>();
        }
        return items;
    }

    public void setSalesOrderItems(List<SalesOrderItem> salesOrderItems) {
        this.items = salesOrderItems;
    }

    @Override
    public String toString() {
        return "SalesOrder{" + "number=" + number + ", date=" + date + ", amount=" + amount + ", status=" + status + ", address=" + address + ", creditCard=" + creditCard + ", customer=" + customer + ", salesOrderItems=" + items + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.number);
        hash = 97 * hash + Objects.hashCode(this.date);
        hash = 97 * hash + Objects.hashCode(this.amount);
        hash = 97 * hash + Objects.hashCode(this.status);
        hash = 97 * hash + Objects.hashCode(this.address);
        hash = 97 * hash + Objects.hashCode(this.creditCard);
        hash = 97 * hash + Objects.hashCode(this.customer);
        hash = 97 * hash + Objects.hashCode(this.items);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SalesOrder other = (SalesOrder) obj;
        if (!Objects.equals(this.number, other.number)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.amount, other.amount)) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.creditCard, other.creditCard)) {
            return false;
        }
        if (!Objects.equals(this.customer, other.customer)) {
            return false;
        }
        if (!Objects.equals(this.items, other.items)) {
            return false;
        }
        return true;
    }
}
