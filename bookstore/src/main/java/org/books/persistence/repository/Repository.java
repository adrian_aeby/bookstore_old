/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence.repository;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Adrian
 */
public class Repository<T> {
    
    @PersistenceContext
    protected EntityManager entityManager;

    public void setEntityManager(EntityManager em) {
        entityManager = em;
    }
    
    public Repository() {}

    public boolean delete(Class<T> type, Object id) {
        T t = find(type, id);
        if (t == null) {
            return false;
        } else {
            entityManager.remove(t);
            return true;
        }
    }

    public T find(Class<T> type, Object id) {
        return entityManager.find(type, id);
    }

    public void persist(T entity) {
        entityManager.persist(entity);
    }

    public T update(T entity) {
        return entityManager.merge(entity);
    }
}
