/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence.repository;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.books.persistence.dto.CustomerInfo;
import org.books.persistence.entity.Customer;

/**
 *
 * @author Adrian
 */
@Stateless
public class CustomerRepository extends Repository<Customer> {

    public CustomerRepository() {
    }

    public Customer find(Long customerNr) {
        return find(Customer.class, customerNr);
    }

    public Customer find(String email) {
        if (email != null) {
            email = email.toLowerCase();
        }
        TypedQuery<Customer> query = entityManager.createNamedQuery(Customer.FIND_BY_EMAIL, Customer.class);
        query.setParameter("email", email);

        List<Customer> list = query.getResultList();
        switch (list.size()) {
            case 1: return list.get(0);
            case 0: return null;
            default: throw new NonUniqueResultException("mehrere Resultate für Kunden mit email = [" + email + "] gefunden!");
        }
    }

    public List<CustomerInfo> search(String name) {
        Query query = entityManager.createQuery(CustomerInfo.SEARCH_BY_FIRST_OR_LASTNAME);
        query.setParameter("parameter", name.toUpperCase());

        return query.getResultList();
    }
}
