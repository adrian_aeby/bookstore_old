/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence.repository;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.books.persistence.dto.BookInfo;
import org.books.persistence.entity.Book;
import org.books.persistence.entity.Book_;

/**
 *
 * @author baechlerf
 */
@Stateless
public class BookRepository extends Repository<Book> {
    
    public BookRepository() {        
    }
    
    public Book find(String isbn){
        if (isbn != null) {
            isbn = isbn.toUpperCase();
        }
        return find(Book.class, isbn);
    } 
    
    public List<BookInfo> search(String[] keywords) {
        
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<BookInfo> cq = cb.createQuery(BookInfo.class);
        Root<Book> book = cq.from(Book.class);
        Expression<String> concatenat = cb.concat(
                cb.concat(
                        cb.upper(book.get(Book_.title)), 
                        cb.upper(book.get(Book_.authors))
                ), cb.upper(book.get(Book_.publisher))
        );
        
        Predicate[] criterias = new Predicate[keywords.length];
        for (int i = 0; i < keywords.length; i++) {
            String keyword = keywords[i];
            criterias[i] = (cb.like(concatenat, keyword.toUpperCase()));
        }

        cq.select(cb.construct(BookInfo.class, book.get(Book_.isbn), book.get(Book_.title), book.get(Book_.price) )).where(criterias);
        TypedQuery<BookInfo> query = entityManager.createQuery(cq);
        
        return query.getResultList();
    }
}
