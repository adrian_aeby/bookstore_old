/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence.repository;

import javax.ejb.Stateless;
import org.books.persistence.entity.Login;

/**
 *
 * @author Adrian
 */
@Stateless
public class LoginRepository extends Repository<Login>{

    public LoginRepository() {
    }
    
    public Login find(String userName) {
        if (userName != null) {
            userName = userName.toLowerCase();
        }
        return find(Login.class, userName);
    }
}
