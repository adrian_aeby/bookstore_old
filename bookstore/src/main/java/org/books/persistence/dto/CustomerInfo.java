/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence.dto;

import java.io.Serializable;

/**
 *
 * @author Adrian
 */
public class CustomerInfo implements Serializable {

    public final static String SEARCH_BY_FIRST_OR_LASTNAME = 
            "SELECT NEW " + CustomerInfo.class.getName() + "(c.number, c.firstName, c.lastName, c.email) "
            + "FROM Customer c where upper(c.firstName) like :parameter or upper(c.lastName) like :parameter";
    
    private Long number;
    private String firstName;
    private String lastName;
    private String email;
    
    public CustomerInfo() {        
    }
    
    public CustomerInfo(Long number, String firstName, String lastName, String email) {
        this.number = number;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public Long getNumber() {
        return number;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "CustomerInfoDTO{" + "number=" + number + ", lastname=" + lastName + ", firstname=" + firstName + ", email=" + email + '}';
    } 
}
