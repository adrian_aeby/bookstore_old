/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.service;

import java.util.List;
import javax.ejb.Remote;
import org.books.application.exception.BookAlreadyExistsException;
import org.books.application.exception.BookNotFoundException;
import org.books.persistence.dto.BookInfo;
import org.books.persistence.entity.Book;

/**
 *
 * @author Adrian
 */
@Remote
public interface CatalogService {
    /**
     * Adds a book to the catalog.
     * @param book the data of the book to be added
     * @throws BookAlreadyExistsException if a book with the same ISBN number already exists
     */
   void	addBook(Book book) throws BookAlreadyExistsException;

   /**
    * Finds a book by its ISBN number.
    * @param isbn the ISBN number of the book
    * @return the data of the found book
     * @throws BookNotFoundException if no book with the specified ISBN number exists
    */
    Book findBook(String isbn) throws BookNotFoundException;
    
    /**
     * Searches for books by keywords.
     * A book is included in the result list if every keyword is contained in its title, authors or publisher field.
     * @param keywords the keywords to search for
     * @return a list of matching books (may be empty)
     */
    List<BookInfo> searchBooks(String keywords);
    
    /**
     * Updates the data of a book. 
     * @param book the data of the book to be updated
     * @throws BookNotFoundException if no book with the specified ISBN number exists
     */
    void updateBook(Book book) throws BookNotFoundException;
    
    Boolean deleteBook(String isbn) throws BookNotFoundException;
}
