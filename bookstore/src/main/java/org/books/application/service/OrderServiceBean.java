/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.service;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.MapMessage;
import javax.jms.Queue;
import org.books.application.dto.PurchaseOrder;
import org.books.application.dto.PurchaseOrderItem;
import org.books.application.exception.BookNotFoundException;
import org.books.application.exception.CustomerNotFoundException;
import org.books.application.exception.OrderAlreadyShippedException;
import org.books.application.exception.OrderNotFoundException;
import org.books.application.exception.PaymentFailedException;
import org.books.persistence.dto.OrderInfo;
import org.books.persistence.entity.Book;
import org.books.persistence.entity.CreditCard;
import org.books.persistence.entity.Customer;
import org.books.persistence.entity.SalesOrder;
import org.books.persistence.entity.SalesOrderItem;
import org.books.persistence.enumeration.OrderStatus;
import org.books.persistence.repository.OrderRepository;

/**
 *
 * @author Adrian
 */
@Stateless(name = "OrderService")
public class OrderServiceBean implements OrderService {
    
    @EJB
    OrderRepository orderRepository;
    
    @EJB
    CustomerService customerService;
    
    @EJB
    CatalogService catalogService;
    
    @Inject
    @JMSConnectionFactory("jms/connectionFactory")
    private JMSContext jmsContext;
    @Resource(lookup = "jms/orderQueue")
    private Queue queue;
    
    /**
     * Cancels an order.
     * @param orderNr the number of the order
     * @throws OrderNotFoundException if no order with the specified number exists
     * @throws OrderAlreadyShippedException  if the order has already been shipped
     */
    public void cancelOrder(Long orderNr) throws OrderNotFoundException, OrderAlreadyShippedException {
        SalesOrder order = findOrder(orderNr);
        switch(order.getStatus()) {
            case SHIPPED: throw new OrderAlreadyShippedException();
            case CANCELED: return; // no updade neccessary
            default: 
                order.setStatus(OrderStatus.CANCELED);
                orderRepository.persist(order);
        }
    }

    /**
     * Finds an order by number.
     * @param orderNr the number of the order
     * @return the data of the found order
     * @throws OrderNotFoundException if no order with the specified number exists
     */
    public SalesOrder findOrder(Long orderNr) throws OrderNotFoundException {
        SalesOrder order = orderRepository.find(orderNr);
        
        if (order == null) {
            throw new OrderNotFoundException();
        }
        
        return order;
    }
    
    /**
     * Places an order on the bookstore.
     * @param purchaseOrder the data of the order to be placed
     * @return the data of the placed order
     * @throws CustomerNotFoundException if no customer with the specified number exists
     * @throws BookNotFoundException if an order item references a non-existent book
     * @throws PaymentFailedException if a payment error occurred
     */
    public SalesOrder placeOrder(PurchaseOrder purchaseOrder) throws CustomerNotFoundException, BookNotFoundException, PaymentFailedException {
        Customer customer = customerService.findCustomer(purchaseOrder.getCustomerNr());
        checkCreditCard(customer.getCreditCard());
        BigDecimal amount = new BigDecimal("0");
        List<SalesOrderItem> items = new ArrayList<>();
        for (PurchaseOrderItem poItem : purchaseOrder.getItems()) {
            Book book = catalogService.findBook(poItem.getBookInfo().getIsbn());
            // was wenn book.price nicht gleich poItem.bookInfo.price ist? --> gilt als gegeben.
            Integer quantity = poItem.getQuantity();
            BigDecimal price = poItem.getBookInfo().getPrice();
            SalesOrderItem item = new SalesOrderItem(book, quantity, price);
            items.add(item);
            amount = amount.add(price.multiply(new BigDecimal(quantity)));
        }
        Date now = new Date(System.currentTimeMillis());
        SalesOrder order = new SalesOrder(now, amount, OrderStatus.ACCEPTED, customer, customer.getAddress(), 
                customer.getCreditCard(), items);
        orderRepository.persist(order);
        try {
            sendMessage(order.getNumber());
        } catch (JMSException ex) {
            Logger.getLogger(OrderServiceBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return order;
    }

    /**
     * Searches for orders by customer and year.
     * @param customerNr the number of the customer
     * @param year the year of the orders
     * @return a list of matching orders (may be empty)
     * @throws CustomerNotFoundException if no customer with the specified email address exists
     */
    public List<OrderInfo> searchOrders(Long customerNr, Integer year) throws CustomerNotFoundException {
        Customer customer = customerService.findCustomer(customerNr);
        return orderRepository.search(customer, year);
    }

    private void sendMessage(Long number) throws JMSException {
        JMSProducer producer = jmsContext.createProducer();
        MapMessage msg = jmsContext.createMapMessage();
        msg.setLong("orderNumber", number);
        producer.send(queue, msg);
    }

    static void checkCreditCard(CreditCard creditCard) throws PaymentFailedException {
        // type
        if (creditCard.getType() == null) {
            throw new PaymentFailedException(PaymentFailedException.Code.INVALID_CREDIT_CARD);
        }
        // expiration date
        LocalDate now = LocalDate.now();
        Integer yearNow = now.getYear();
        Integer monthNow = now.getMonthValue();
        Integer yearExp = creditCard.getExpirationYear();
        Integer monthExp = creditCard.getExpirationMonth();
        if (yearExp == null || monthExp == null) {
            throw new PaymentFailedException(PaymentFailedException.Code.CREDIT_CARD_EXPIRED);
        }
        if (yearNow < yearExp) {
            return; // everything ok
        } else if (yearNow > yearExp) {
            throw new PaymentFailedException(PaymentFailedException.Code.CREDIT_CARD_EXPIRED);
        } else if (monthNow <= monthExp) {
            return; // everything ok
        } else {
            throw new PaymentFailedException(PaymentFailedException.Code.CREDIT_CARD_EXPIRED);
        }
    }
}
