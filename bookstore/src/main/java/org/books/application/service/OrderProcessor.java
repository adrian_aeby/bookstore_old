/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.service;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.books.application.exception.OrderNotFoundException;
import org.books.persistence.entity.SalesOrder;
import org.books.persistence.enumeration.OrderStatus;

/**
 *
 * @author Adrian
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/orderQueue"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class OrderProcessor implements MessageListener {
    @EJB
    private OrderService orderService;
    @EJB
    private MailBean mail;
    @Resource
    private TimerService timerService;
    @Resource(name = "waitTime") 
    private long waitTime;
    @Resource(name = "mail.to") 
    private String to;
    @PersistenceContext
    protected EntityManager entityManager;
    
    @Override
    public void onMessage(Message message) {
        try {
            MapMessage msg = (MapMessage) message;
            Long orderNumber = msg.getLong("orderNumber");
            SalesOrder order = orderService.findOrder(orderNumber);
            if (order.getStatus().equals(OrderStatus.ACCEPTED)) { // TODO in mail an Stephan beschreiben
                order.setStatus(OrderStatus.PROCESSING);
                mail.sendMail(to, "Bestellung "+orderNumber+" wird verarbeitet", "body");
                entityManager.merge(order);
                timerService.createSingleActionTimer(waitTime, new TimerConfig(orderNumber, true));
            } else {
                Logger.getLogger(OrderProcessor.class.getName()).log(Level.WARNING, "Bestellung hat falschen Status: Nummer={0}, Status={1}", new Object[]{orderNumber, order.getStatus()});
            }
        } catch (Exception ex) {
            Logger.getLogger(OrderProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
    @Timeout
    public void finishOrder(Timer timer) {
        try {
            Long orderNumber = (Long) timer.getInfo();
            SalesOrder order = orderService.findOrder(orderNumber);
            if (order.getStatus().equals(OrderStatus.PROCESSING)) { // TODO in mail an Stephan beschreiben
                order.setStatus(OrderStatus.SHIPPED);
                entityManager.merge(order);
                mail.sendMail(to, "Bestellung "+orderNumber+" wurde versandt", "body");
            } else {
                // TODO Log
                Logger.getLogger(OrderProcessor.class.getName()).log(Level.WARNING, "Bestellung hat falschen Status: Nummer={0}, Status={1}", new Object[]{orderNumber, order.getStatus()});
            }
        } catch (OrderNotFoundException ex) {
            Logger.getLogger(OrderProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
