/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.service;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.books.application.exception.BookAlreadyExistsException;
import org.books.application.exception.BookNotFoundException;
import org.books.integration.amazon.AmazonCatalog;
import org.books.persistence.dto.BookInfo;
import org.books.persistence.entity.Book;
import org.books.persistence.repository.BookRepository;

/**
 *
 * @author baechlerf
 */
@Stateless(name = "CatalogService")
public class CatalogServiceBean implements CatalogService{

    @EJB
    private BookRepository bookRepository;
    @EJB
    private AmazonCatalog amazonCatalog;
    
    @Override
    public void addBook(Book book) throws BookAlreadyExistsException {
     
        Book result = bookRepository.find(book.getIsbn());
        
        if (result == null) {
            bookRepository.persist(book);
        } else {
            
            throw new BookAlreadyExistsException();
        }
    }

    @Override
    public Book findBook(String isbn) throws BookNotFoundException {

        return amazonCatalog.itemLookup(isbn);
    }

    @Override
    public List<BookInfo> searchBooks(String keywords) {
        
        if (keywords == null || keywords.trim().isEmpty()) {
            return Collections.EMPTY_LIST;
        }
               
        try {
            return amazonCatalog.itemSearch(keywords);
        } catch (BookNotFoundException ex) {
            return Collections.emptyList();
        }
       
    }

    @Override
    public void updateBook(Book book) throws BookNotFoundException {
        Book original = findBook(book.getIsbn());
        if (original == null) {
            throw new BookNotFoundException();
        } else {
            bookRepository.update(book);
        }
    }

    @Override
    public Boolean deleteBook(String isbn) throws BookNotFoundException {
        Book result = bookRepository.find(isbn);
        
        if (result == null) {
            
            throw new BookNotFoundException();
            
        } else {
            
            return bookRepository.delete(Book.class, isbn);
        }
        
    }
    
}
