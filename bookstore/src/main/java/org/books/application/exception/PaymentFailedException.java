/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.exception;

/**
 *
 * @author Adrian
 */
public class PaymentFailedException extends BookstoreException {
    private Code code;

    public PaymentFailedException(Code code) {
        this.code = code;
    }

    public Code getCode() {
        return code;
    }
    
    public static enum Code {
        CREDIT_CARD_EXPIRED,
        INVALID_CREDIT_CARD,
        PAYMENT_LIMIT_EXCEEDED,
    }
}
