/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bfh.eadj.pla.bookstore.order;

import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author Adrian
 */
public class OrderStatistic {
/* Dieses Query verursacht zwei zusätzliche Select Statements um den User zu lesen.
            "SELECT NEW " + OrderStatistic.class.getName() + "("
          + "c, SUM(i.price), COUNT(i), AVG(i.price)) "
          + "FROM SalesOrderEntity o JOIN o.customer c JOIN o.salesOrderItems i "
          + "WHERE o.date between :from and :to "
          + "GROUP BY c";    
    c wird in ein Feld CustomerEntity geladen.
*/
    
    
    public final static String GET_STATISTIC_OF_ALL_CUSTOMER_FOR_ONE_YEAR = 
            "SELECT NEW " + OrderStatistic.class.getName() + "("
          + "c.number, c.firstName, c.lastName, SUM(i.price), COUNT(i), AVG(i.price)) "
          + "FROM SalesOrder o JOIN o.customer c JOIN o.items i "
          + "WHERE o.date between :from and :to "
          + "GROUP BY c.number, c.firstName, c.lastName";
    
    private final Long number;
    private final String firstName;
    private final String lastName;
    private final BigDecimal sum;
    private final Long count;
    private final Double avg;

    public OrderStatistic(Long number, String firstName, String lastName, BigDecimal sum, Long count, Double avg) {
        this.number = number;
        this.firstName = firstName;
        this.lastName = lastName;
        this.sum = sum;
        this.count = count;
        this.avg = avg;
    }

    public Long getNumber() {
        return number;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public Long getCount() {
        return count;
    }

    public double getAvg() {
        return avg;
    }

    @Override
    public String toString() {
        return "OrderStatistic{" + "number=" + number + ", firstName=" + firstName + ", lastName=" + lastName + ", sum=" + sum + ", count=" + count + ", avg=" + avg + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.number);
        hash = 53 * hash + Objects.hashCode(this.firstName);
        hash = 53 * hash + Objects.hashCode(this.lastName);
        hash = 53 * hash + Objects.hashCode(this.sum);
        hash = 53 * hash + Objects.hashCode(this.count);
        hash = 53 * hash + Objects.hashCode(this.avg);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrderStatistic other = (OrderStatistic) obj;
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.number, other.number)) {
            return false;
        }
        if (!Objects.equals(this.sum, other.sum)) {
            return false;
        }
        if (!Objects.equals(this.count, other.count)) {
            return false;
        }
        if (!Objects.equals(this.avg, other.avg)) {
            return false;
        }
        return true;
    } 
}
