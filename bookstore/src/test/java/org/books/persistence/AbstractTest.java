/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence;


import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.books.persistence.entity.Address;
import org.books.persistence.entity.Book;
import org.books.persistence.entity.CreditCard;
import org.books.persistence.entity.Customer;
import org.books.persistence.entity.Login;
import org.books.persistence.entity.SalesOrder;
import org.books.persistence.entity.SalesOrderItem;
import org.books.persistence.enumeration.BookBinding;
import org.books.persistence.enumeration.CreditCardType;
import org.books.persistence.enumeration.OrderStatus;
import org.books.persistence.enumeration.UserGroup;
import org.books.persistence.repository.BookRepository;
import org.books.persistence.repository.CustomerRepository;
import org.books.persistence.repository.LoginRepository;
import org.books.persistence.repository.OrderRepository;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author baechlerf
 */
public abstract class AbstractTest {
      
    protected final static Logger LOGGER = Logger.getLogger(AbstractTest.class.getName());
    
    private final static String DB_NAME = "bookstore";

    protected static EntityManagerFactory emf;
    protected static EntityManager em;
    
    // Repositories
   // protected static UserRepository userRepository = new UserRepository();
    protected static LoginRepository loginRepository = new LoginRepository();
    protected static BookRepository bookRepository = new BookRepository();
    protected static CustomerRepository customerRepository = new CustomerRepository();
    protected static OrderRepository orderRepository = new OrderRepository();
   

    public static UserGroup customerGroup;
    public static UserGroup staffGroup;
    public static Login staffUser1;
    public static Login staffUser2;
    public static Login hansSutterUser;
    public static Login fritzMeierUser;
    public static Customer hansSutter;
    public static Customer fritzMeier;
    public static Address addressBern;
    public static CreditCard masterCard1;
    public static Address addressFreiburg;
    public static CreditCard amex1;
    public static Book book1;
    public static Book book2;
    public static Book book3;
    public static SalesOrder hs2016_1Order;
    public static SalesOrder hs2016_2Order;
    public static SalesOrder hs2015_1Order;
    public static SalesOrder fm2015_1Order;
    public static SalesOrder fm2015_2Order;
    public static SalesOrder fm2016_1Order;
    public static SalesOrder fm2016_2Order;
    public static Set<SalesOrder> orders2010;
    public static Set<SalesOrder> orders2015;
    public static Set<SalesOrder> orders2016;
    public static Set<SalesOrder> allOrders;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        emf = Persistence.createEntityManagerFactory(DB_NAME);
        em = emf.createEntityManager();
        
        loginRepository.setEntityManager(em);
        bookRepository.setEntityManager(em);
        customerRepository.setEntityManager(em);
        orderRepository.setEntityManager(em);
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        
        if (em != null) {
            em.close();
        }
        if (emf != null) {
            emf.close();
        }
    }
    
    @Before
    public void cleanDB() {
        
        em.getTransaction().begin();
        
        int deletedFromSalesOrderItemEntity = em.createQuery("DELETE FROM SalesOrderItem").executeUpdate();
        int deletedFromSalesOrder = em.createQuery("DELETE FROM SalesOrder").executeUpdate();
        int deletedFromBook = em.createQuery("DELETE FROM Book").executeUpdate();
        int deletedFromCustomer = em.createQuery("DELETE FROM Customer").executeUpdate();
        int deletedFromUser = em.createQuery("DELETE FROM Login").executeUpdate();
        
        em.getTransaction().commit();
       
        StringBuilder sb = new StringBuilder();
        
        sb.append("Deleted from:\n");
        sb.append("BookEntity: " + deletedFromBook + "\n");
        sb.append("CustomerEntity: " + deletedFromCustomer + "\n");
        sb.append("SalesOrder: " + deletedFromSalesOrder + "\n");
        sb.append("SalesOrderItemEntity: " + deletedFromSalesOrderItemEntity + "\n");
        sb.append("UserEntity: " + deletedFromUser + "\n");
        
        System.out.println(sb);
        
        // Dates
        Date d2016 = null;
        Date d2015 = null;
        try {
            d2016 = getDate("01.01.2016");
            d2015 = getDate("01.01.2015");
        } catch (ParseException ex) {
            Logger.getLogger(AbstractTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Test DB mit Daten abfüllen
        em.getTransaction().begin(); 
        
        hansSutterUser = new Login("hans.sutter@hs.ch", "pwd", UserGroup.CUSTOMER); 
                loginRepository.persist(hansSutterUser);
        customerGroup = hansSutterUser.getUserGroup();
        
        fritzMeierUser = new Login("fritz.meier@fm.ch", "pwd", UserGroup.CUSTOMER);
                loginRepository.persist(fritzMeierUser);
                
        staffUser1 = new Login("admin@admin1.ch", "pwd", UserGroup.EMPLOYEE);
                loginRepository.persist(staffUser1);
        staffGroup = staffUser1.getUserGroup();
        
        staffUser2 = new Login("admin@admin2.ch", "pwd", UserGroup.EMPLOYEE);
                loginRepository.persist(staffUser2);
        
        book1 = new Book("isbn-1", "JPA EclipseLink Hibernate", "Tom Hiller", "O'Reily", 2011, BookBinding.HARDCOVER,100, new BigDecimal("19.90"));
                bookRepository.persist(book1);  
        book2 = new Book("isbn-2", "EJB Session Bean", "Jerry Miller", "APress", 2011, BookBinding.PAPERBACK, 100, new BigDecimal("31.10"));
                bookRepository.persist(book2);
        book3 = new Book("isbn-3", "Angular JavaScript", "Tom Hanks", "APress",2011, BookBinding.EBOOK, 100, new BigDecimal("28.50"));
                bookRepository.persist(book3);        
        
        addressBern = new Address("Hauptstrasse 5", "3000", "Bern", "Schweiz");
        masterCard1 = new CreditCard( CreditCardType.MASTERCARD, "1000-1000", 12, 2017);
        hansSutter = new Customer("Hans", "Sutter", "hans.sutter@hs.ch", addressBern, masterCard1);
                customerRepository.persist(hansSutter);
                
        addressFreiburg = new Address("Bahnhofstrasse 5", "1700", "Freiburg", "Schweiz");
        amex1 =  new CreditCard(CreditCardType.VISA, "10-5000-20", 12, 2017);
        fritzMeier = new Customer("Fritz", "Meier", "fritz.meier@fm.ch", addressFreiburg, amex1);
                customerRepository.persist(fritzMeier);
        
        
        List<SalesOrderItem> salesOrderItems = createSalesOrderItemEntities(book1, book2, book2, book3);
        //TODO amount fix im Konstruktor übergeben, eventuell noch nachfragen
        hs2016_1Order = new SalesOrder(d2016, new BigDecimal("3.40"), OrderStatus.SHIPPED, hansSutter, addressBern, masterCard1, salesOrderItems);
                orderRepository.persist(hs2016_1Order);
                
        salesOrderItems = createSalesOrderItemEntities(book1, book2, book2, book3);    
        hs2016_2Order = new SalesOrder(d2016, new BigDecimal("34.40"),  OrderStatus.SHIPPED, hansSutter, addressBern, masterCard1, salesOrderItems); 
                orderRepository.persist(hs2016_2Order);
                
        salesOrderItems = createSalesOrderItemEntities(book2, book3);
        hs2015_1Order = new SalesOrder(d2015, new BigDecimal("20.30"), OrderStatus.SHIPPED, hansSutter, addressBern, masterCard1, salesOrderItems);
                orderRepository.persist(hs2015_1Order);
        
        salesOrderItems = createSalesOrderItemEntities(book1, book2, book3, book3);
        fm2015_1Order = new SalesOrder(d2015, new BigDecimal("15.90"), OrderStatus.SHIPPED, fritzMeier, addressFreiburg, amex1, salesOrderItems) ;
                orderRepository.persist(fm2015_1Order);
        
        salesOrderItems = createSalesOrderItemEntities(book1, book3);
        fm2015_2Order = new SalesOrder(d2015,new BigDecimal("19.40"), OrderStatus.SHIPPED, fritzMeier, addressFreiburg, amex1, salesOrderItems);
        
        salesOrderItems = createSalesOrderItemEntities(book2);
        fm2016_1Order = new SalesOrder(d2016, new BigDecimal("34.00"), OrderStatus.SHIPPED, fritzMeier, addressFreiburg, amex1, salesOrderItems);
        
        salesOrderItems = createSalesOrderItemEntities(book1, book2, book3, book3);
        fm2016_2Order = new SalesOrder(d2016, new BigDecimal("103.60"), OrderStatus.SHIPPED, fritzMeier,  addressFreiburg, amex1, salesOrderItems);
        
        orders2010 = new HashSet<>();
        orders2015 = new HashSet<>();
        orders2015.add(hs2015_1Order);
        orders2015.add(fm2015_1Order);
        orders2015.add(fm2015_2Order);
        orders2016 = new HashSet<>();
        orders2016.add(hs2016_1Order);
        orders2016.add(hs2016_2Order);
        orders2016.add(fm2016_1Order);
        orders2016.add(fm2016_2Order);
        
        allOrders = new HashSet<>();
        allOrders.addAll(orders2015);
        allOrders.addAll(orders2016);

        
        em.getTransaction().commit();
        em.clear();
        emf.getCache().evictAll();
    }

    public static List<SalesOrderItem> createSalesOrderItemEntities(Book... books) {
        Map<Book, Integer> bookMap = new HashMap<>();
        for (Book book : books) {
            Integer count = bookMap.get(book);
            if (count == null) {
                bookMap.put(book, 1);
            } else {
                count++;
            }            
        }
        List<SalesOrderItem> result = new ArrayList<>();
        for (Book book : bookMap.keySet()) {
            SalesOrderItem item = new SalesOrderItem(book, bookMap.get(book), book.getPrice() );
            result.add(item);
        }
        return result;
    }
    
    public static Date getDate(String date) throws ParseException {
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        return new Date(df.parse(date).getTime());
    }
}
