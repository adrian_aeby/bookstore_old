/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence;


import java.util.List;
import javax.persistence.NoResultException;
import org.books.persistence.dto.BookInfo;
import org.books.persistence.entity.Book;
import org.books.persistence.repository.BookRepository;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author baechlerf
 */
public class BookTest extends AbstractTest{
    
    @Test
    public void getBookInfoByISBNPositiv(){
       
       // arrange
       // act
       Book result = bookRepository.find(book1.getIsbn());
       
       // assert
       Assert.assertEquals("ISBN stimmt nicht!",book1.getIsbn(), result.getIsbn());
       Assert.assertEquals("Preis stimmt nicht!",book1.getPrice(), result.getPrice());
       Assert.assertEquals("Title stimmt nicht!",book1.getTitle(), result.getTitle());
    }
    
    @Test
    public void getBookEntityByISBNNegativ() {
        
        // arrange
        // act
        Book result = bookRepository.find("GIBT_ES_NICHT");
        
        // arrang
        Assert.assertNull(result);
    }
    
    @Test
    public void getBookInfoByEmptyParameter() {
        
        // arrange
        // act
        Book result = bookRepository.find("");
        
        //
        Assert.assertNull(result);
    }
   
    @Test (expected = IllegalArgumentException.class)
    public void getBookInfoByNullParameter() {
        
        // arrange
        // act
        Book result = bookRepository.find(null);
        
        // assert
        Assert.fail();
    }
    
    @Test
    public void searchBooksWithContainKeywordsOneKeywordPositiv() {
        
        // arrange
        // Bemerkung Keyword muss % Zeichen enthalten sonst geht es nicht!
        String[] input = {"%APress%"};
        
        // act
        List<BookInfo> result = bookRepository.search(input);
        
        // assert
        Assert.assertEquals("Anzahl Bücher stimmt nicht!", 2, result.size());
    }
    
    @Test
    public void searchBooksWithContainKeywordsOneKeywordPositivCaseInsensitive() {
        
        // arrange
        // Bemerkung Keyword muss % Zeichen enthalten sonst geht es nicht!
        String[] input = {"%apress%"};
        
        // act
        List<BookInfo> result = bookRepository.search(input);
        
        // assert
        Assert.assertEquals("Anzahl Bücher stimmt nicht!", 2, result.size());
    }
    
    @Test (expected = NullPointerException.class)
    public void searchBooksWithContainKeywordsNullValue() {
        
        // arrange
        String[] input = {null};
        
        // act
        bookRepository.search(input);
        
        // assert
        Assert.fail();
    }
    
    @Test
    public void searchBooksWithContainKeywordsPositiv() {
        
        // arrange
        String[] input = {"%APress%","%Tom%"};
        
        // act
        List<BookInfo> result = bookRepository.search(input);
        
        // assert
        Assert.assertEquals("Anzahl Bücher stimmt nicht!", 1, result.size());      
    }
    
    @Test
    public void searchBooksWithContainKeywordsTreeKeywordsPositiv() {
        
        // arrange
        String[] input = {"%APress%","%Tom%","%Java%"};
        
        // act
        List<BookInfo> result = bookRepository.search(input);
        
        // assert
        Assert.assertEquals("Anzahl Bücher stimmt nicht!", 1, result.size());       
    }
    
    @Test
    public void searchBooksWithContainKeywordsNegativ() {
        
        // arrange
        String[] input = {"%APress%","%Tom%","%GIBT_ES_NICHT%"};
        
        // act
        List<BookInfo> result = bookRepository.search(input);
        
        // assert
        Assert.assertEquals("Anzahl Bücher stimmt nicht!", 0, result.size());    
    }
   
    @Test
    public void searchBooksWithContainKeywordsdWithOutWildcardsPositiv() {
        
        // arrange
        // Bemerkung Keyword muss % Zeichen enthalten sonst geht es nicht!
        
        // mit "APress" findet er nichts und auch mit "APress%" findet er nichts
        String[] input = {"APress"};
        
        // act
        List<BookInfo> result = bookRepository.search(input);
        
        // assert
        Assert.assertEquals("Anzahl Bücher stimmt nicht!", 0, result.size());
    }
}

