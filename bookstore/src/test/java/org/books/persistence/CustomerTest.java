/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence;


import java.util.List;
import org.books.persistence.dto.CustomerInfo;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author baechlerf
 */
public class CustomerTest extends AbstractTest{
     
    @Test
    public void searchCustomerInfoPositiv() {
     
        // arramge
        String name = "%e%";
        
        // act
        List<CustomerInfo> result = customerRepository.search(name);
        
        // arrange
        Assert.assertEquals("Anzahl CustomerInfo stimmt nicht!", 2, result.size());            
    }
    
    @Test
    public void searchCustomerInfoNegativ() {
        
        // arragne
        String name = "%GIBT_ES_NICHT%";
        
        // act 
        List<CustomerInfo> result = customerRepository.search(name);
        
        // arrange
        Assert.assertEquals("Anzahl CustomerInfo stimmt nicht!", 0, result.size()); 
    }
    
    @Test
    public void searchCustomerInfoFirstNamePositiv() {
        
         // arragne
        String name = "%Fritz%";
        
        // act 
        List<CustomerInfo> result = customerRepository.search(name);
        
        // arrange
        Assert.assertEquals("Anzahl CustomerInfo stimmt nicht!", 1, result.size()); 
        Assert.assertEquals(fritzMeier.getFirstName(), result.get(0).getFirstName());
    }
    
    @Test
    public void searchCustomerInfoFirstNameCaseInsensitvePositiv() {
        
         // arragne
        String name = "%fritz%";
        
        // act 
        List<CustomerInfo> result = customerRepository.search(name);
        
        // arrange
        Assert.assertEquals("Anzahl CustomerInfo stimmt nicht!", 1, result.size()); 
        Assert.assertEquals(fritzMeier.getFirstName(), result.get(0).getFirstName());
    }
    
    @Test
    public void searchCustomerInfoLastNamePositiv() {
        
         // arragne
        String name = "%Mei%";
        
        // act 
        List<CustomerInfo> result = customerRepository.search(name);
        
        // arrange
        Assert.assertEquals("Anzahl CustomerInfo stimmt nicht!", 1, result.size()); 
        Assert.assertEquals(fritzMeier.getLastName(), result.get(0).getLastName());
    }
    
    @Test
    public void searchCustomerInfoLastNameWithTreePrecentSignPositiv() {
        
         // arragne
        String name = "%Me%r%";
        
        // act 
        List<CustomerInfo> result = customerRepository.search(name);
        
        // arrange
        Assert.assertEquals("Anzahl CustomerInfo stimmt nicht!", 1, result.size()); 
        Assert.assertEquals(fritzMeier.getLastName(), result.get(0).getLastName());
    }
}