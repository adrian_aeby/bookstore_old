/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.Persistence;
import org.books.application.dto.Registration;
import org.books.application.exception.BookAlreadyExistsException;
import org.books.application.exception.BookNotFoundException;
import org.books.application.exception.CustomerAlreadyExistsException;
import static org.books.application.service.Common.createRegistration;
import static org.books.application.service.Common.getBook;
import static org.books.application.service.CustomerServiceIT.em;
import static org.books.application.service.OrderServiceIT.emf;
import org.books.persistence.dto.BookInfo;
import org.books.persistence.entity.Book;
import org.books.persistence.enumeration.BookBinding;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Adrian
 */
public class CatalogServiceIT {
    
    protected final static Logger LOGGER = Logger.getLogger(CatalogServiceIT.class.getName());
    
    private static final String CATALOG_SERVICE_NAME = "java:global/bookstore/CatalogService";
    private static final String CUSTOMER_SERVICE_NAME = "java:global/bookstore/CustomerService";
    
    private final static String DB_NAME = "bookstore";
    
    private static CatalogService catalogService;
    private static CustomerService customerService;
    
    private String isbn;
    
    @BeforeClass
    public static void lookupService() throws NamingException {
        catalogService = (CatalogService) new InitialContext().lookup(CATALOG_SERVICE_NAME);
        customerService = (CustomerService) new InitialContext().lookup(CUSTOMER_SERVICE_NAME);
        
        emf = Persistence.createEntityManagerFactory(DB_NAME);
        em = emf.createEntityManager();
    }
    
    @Before
    public void cleanUp() {
        
        em.getTransaction().begin();
        
        int deletedFromSalesOrderItemEntity = em.createQuery("DELETE FROM SalesOrderItem").executeUpdate();
        int deletedFromSalesOrder = em.createQuery("DELETE FROM SalesOrder").executeUpdate();
        int deletedFromBook = em.createQuery("DELETE FROM Book").executeUpdate();
        int deletedFromCustomer = em.createQuery("DELETE FROM Customer").executeUpdate();
        int deletedFromUser = em.createQuery("DELETE FROM Login").executeUpdate();
        
        em.getTransaction().commit();
       
        StringBuilder sb = new StringBuilder();
        
        sb.append("Deleted from:\n");
        sb.append("BookEntity: " + deletedFromBook + "\n");
        sb.append("CustomerEntity: " + deletedFromCustomer + "\n");
        sb.append("SalesOrder: " + deletedFromSalesOrder + "\n");
        sb.append("SalesOrderItemEntity: " + deletedFromSalesOrderItemEntity + "\n");
        sb.append("UserEntity: " + deletedFromUser + "\n");
        
        System.out.println(sb);  

    }
        
    @Test
    public void findBook() throws BookNotFoundException, BookAlreadyExistsException, CustomerAlreadyExistsException {
        // arrange
        // fillDB();
        LOGGER.log(Level.INFO, "findBook mit ISBN=9781118407813");
        // act 
        Book result = catalogService.findBook("9781118407813");
        
        // assert      
        Assert.assertEquals("Beginning Programming with Java For Dummies", result.getTitle());
    }
    
    @Test
    public void findBook2Results() throws BookNotFoundException, BookAlreadyExistsException, CustomerAlreadyExistsException {
        // arrange
        // fillDB();
        LOGGER.log(Level.INFO, "findBook mit ISBN=0596009208");
        // act 
        Book result = catalogService.findBook("0596009208");
        
        // assert      
        Assert.assertEquals("Head First Java, 2nd Edition", result.getTitle());
    }
    
    @Test(expected = BookNotFoundException.class)
    // scheint zu funktionieren fb 21.01.2017
    
    // TODO AA 20.01.2017 es soll die BookNotFoundException kommen
//    Caused by: java.lang.RuntimeException: ISBN-GIBTESNICHT is not a valid value for ItemId. Please change this value and retry your request.
//	at org.books.integration.amazon.AmazonCatalog.itemLookup(AmazonCatalog.java:78)
    public void findBookNotFound() throws BookNotFoundException {
        // arrange
        String isbn = "isbn-gibtEsNicht";
        // act 
        catalogService.findBook(isbn);
        // assert
    }
    
    @Test
    public void searchBooksNoKeyword() throws BookAlreadyExistsException, CustomerAlreadyExistsException {
        // arrange
        // fillDB();
        LOGGER.log(Level.INFO, "searchBooksNoKeyword");
        // act
        List<BookInfo> books = catalogService.searchBooks("");
        // assert
        Assert.assertEquals(0, books.size());
    }
    
    @Test
    public void searchBooksTwoKeyword() throws BookAlreadyExistsException, CustomerAlreadyExistsException {
        // arrange
        // fillDB();
        LOGGER.log(Level.INFO, "searchBooksTwoKeyword");
       
        // act
        List<BookInfo> books = catalogService.searchBooks("Matarese ludlum");
        // assert
        Assert.assertEquals(6, books.size());
    }
    
    
    @Test
    public void findBookSlowDown() throws BookNotFoundException {
        
        // arrange
        LOGGER.log(Level.INFO, "findBookSlowDown mit 25 'find' Aufrufen");
        
        // act 
        Book result = null;
        for(int i=0; i < 20; i++) {
            
            result = catalogService.findBook("9781617292897");   
            // assert
            Assert.assertEquals("Testing Java Microservices", result.getTitle());
        }
    }
    
    @Test
    public void findBookSlowDownMoreThreads() throws BookNotFoundException {
    
        Runnable myFirst = new Runnable() {
            @Override
            public void run() {
                
                Book result = null;
                
                System.out.println("Aufruf von Thread First");
                for(int i=0; i < 10; i++) {
                    System.out.println("in der Schleife vom first");
                    try {
                        result = catalogService.findBook("9781617292897");
                    } catch (BookNotFoundException ex) {
                        Logger.getLogger(CatalogServiceIT.class.getName()).log(Level.SEVERE, null, ex);
                    }
            
                    // assert
                    Assert.assertEquals("Testing Java Microservices", result.getTitle());
                }
            }
        };
        
        
         Runnable mySecond = new Runnable() {
            @Override
            public void run() {
                
                Book result = null;
                
                System.out.println("Aufruf von Thread Second");
                for(int i=0; i < 10; i++) {
                    System.out.println("in der Schleife vom second");
                    try {
                        result = catalogService.findBook("9781785882074");
                    } catch (BookNotFoundException ex) {
                        Logger.getLogger(CatalogServiceIT.class.getName()).log(Level.SEVERE, null, ex);
                    }
            
                    // assert
                    Assert.assertEquals("Learning Angular 2", result.getTitle());
                }
            }
        };
        
        Thread threadFirst = new Thread(myFirst);
        Thread threadSecond = new Thread(mySecond);
        
        threadFirst.start();
        threadSecond.start();
     
        
        while(threadFirst.isAlive()){
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                Logger.getLogger(CatalogServiceIT.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        while(threadSecond.isAlive());    
    }
    
    @Test
    public void searchBooksWith100Items() {
        
        LOGGER.log(Level.INFO, "searchBooksWith100Items");
       
        // act
        List<BookInfo> books = catalogService.searchBooks("Java");
        // assert
        // nicht alle 100 Bücher sind valid
        Assert.assertEquals(89, books.size());
    }
    
    @Test
    public void findBookAndSerachBooksOnSameTime() {
       
        Runnable myFirst = new Runnable() {
            @Override
            public void run() {        
                Book result = null;
                
                System.out.println("Aufruf von Thread für findBook");
                for(int i=0; i < 5; i++) {
                    System.out.println("in der Schleife vom findBook");
                    try {
                        result = catalogService.findBook("9781617292897");
                    } catch (BookNotFoundException ex) {
                        Logger.getLogger(CatalogServiceIT.class.getName()).log(Level.SEVERE, null, ex);
                    }
            
                    // assert
                    Assert.assertEquals("Testing Java Microservices", result.getTitle());
                }
            }
        };
        
        
         Runnable mySecond = new Runnable() {
            @Override
            public void run() {
                
                //Book result = null;
                
                System.out.println("Aufruf von Thread searchBooks");
                for(int i=0; i < 2; i++) {
                    System.out.println("in der Schleife vom searchBooks");
                    
                    List<BookInfo> books = catalogService.searchBooks("Java");
                    
                    // assert
                    Assert.assertEquals(89, books.size());
                }
            }
        };
        
        Thread threadFirst = new Thread(myFirst);
        Thread threadSecond = new Thread(mySecond);
        
        threadFirst.start();
        threadSecond.start();
     
        
        while(threadFirst.isAlive()){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(CatalogServiceIT.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        while(threadSecond.isAlive());    
    }
    
     private void fillDB() throws CustomerAlreadyExistsException, BookAlreadyExistsException {

        Registration registration1 = createRegistration("Passwort", "frederic.baechler@test.ch", "Frédéric", "Bächler");
        customerService.registerCustomer(registration1);
        Registration registration2 = createRegistration("pw", "adrian.aeby@test.ch", "Adrian", "Aeby");
        customerService.registerCustomer(registration2);

        Book book1 = getBook("978-3-86894-907-0", "David Barnes", BookBinding.PAPERBACK, 672, new BigDecimal("47.10"), 2013, "Pearson", "Java lernen mit BlueJ");
        catalogService.addBook(book1);
        Book book2 = getBook("978-3-95845-055-4", "Wolfgang Höfer", BookBinding.PAPERBACK, 576, new BigDecimal("45.90"), 2016, "MITP Verlags GmbH & Co. KG", "Raspberry Pi programmieren mit Java");
        catalogService.addBook(book2);
        Book book3 = getBook("978-3-8362-4119-9", "Christian Ullenboom", BookBinding.PAPERBACK, 1312, new BigDecimal("67"), 2016, "Rheinwerk Verlag", "Java ist auch eine Insel");
        catalogService.addBook(book3);
        Book book4 = getBook("111-2-3333-4444-5", "Robert Ludlum", BookBinding.PAPERBACK, 999, new BigDecimal("39.99"), 1999, "X-Publish", "Der Janson Befehl");
        catalogService.addBook(book4);   
    }
}
