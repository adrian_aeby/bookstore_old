/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.service;

import java.math.BigDecimal;
import org.books.application.dto.Registration;
import org.books.persistence.entity.Address;
import org.books.persistence.entity.Book;
import org.books.persistence.entity.CreditCard;
import org.books.persistence.entity.Customer;
import org.books.persistence.enumeration.BookBinding;
import org.books.persistence.enumeration.CreditCardType;

/**
 *
 * @author baechlerf
 */
public class Common {
    
    public static Book getBook(String isbn, String authors, BookBinding binding, Integer numberOfPages, BigDecimal price, Integer publicationYear, String publisher, String title) {
        Book book = new Book();
        book.setIsbn(isbn);
        book.setAuthors(authors);
        book.setBinding(binding);
        book.setNumberOfPages(numberOfPages);
        book.setPrice(price);
        book.setPublicationYear(publicationYear);
        book.setPublisher(publisher);
        book.setTitle(title);
        return book;
    }

    public static Book updateBook(Book original, String isbn, String authors, BookBinding binding, Integer numberOfPages, BigDecimal price, Integer publicationYear, String publisher, String title) {
        Book book = new Book();
        book.setIsbn(isbn);
        book.setAuthors(authors);
        book.setBinding(binding);
        book.setNumberOfPages(numberOfPages);
        book.setPrice(price);
        book.setPublicationYear(publicationYear);
        book.setPublisher(publisher);
        book.setTitle(title);
        book.setVersion(original.getVersion());
        book.setCreatedAt(original.getCreatedAt());
        return book;
    }
     
    public static Registration createRegistration(String passwort, String email, String firstName, String lastName) {
        Registration result = new Registration();
        Customer customer = createCustomer(email, firstName, lastName);
        
        result.setCustomer(customer);
        result.setPassword(passwort);
        
        return result;
    }
    
    private static Customer createCustomer(String email, String firstName, String lastName) {
        
        Customer result = new Customer();
        Address address = createAddress();
        CreditCard creditCard = createCreditCard();
        
        result.setAddress(address);
        result.setCreditCard(creditCard);
        result.setEmail(email);
        result.setFirstName(firstName);
        result.setLastName(lastName);
        
        return result;
    }
    
    private static Address createAddress() {
        
        Address result = new Address("Dorfstrasse 8", "Plasselb", "1737", "Schweiz");
        
        return result;
    }
    
    private static CreditCard createCreditCard() {
        
        CreditCard result = new CreditCard(CreditCardType.VISA, "NUMMER-1234", new Integer("10"), new Integer("2017"));
        
        return result;
    }
}
