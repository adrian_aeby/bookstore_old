/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.service;


import java.util.List;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.books.application.dto.Registration;
import org.books.application.exception.CustomerAlreadyExistsException;
import org.books.application.exception.CustomerNotFoundException;
import org.books.application.exception.InvalidPasswordException;
import static org.books.application.service.Common.createRegistration;
import org.books.persistence.dto.CustomerInfo;
import org.books.persistence.entity.Customer;
import org.books.persistence.enumeration.CreditCardType;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 *
 * @author baechlerf
 */
public class CustomerServiceIT{

    private Logger LOGGER = Logger.getLogger(CustomerServiceIT.class.getName());
    
    private static final String CUSTOMER_SERVICE_NAME = "java:global/bookstore/CustomerService";
    
    private static final String FIRST_NAME = "Frédéric";
    private static final String LAST_NAME = "Bächler";
    private static final String EMAIL = "frederic.baechler@test.ch";
    private static final String PASSWORD = "Passwort";
    
    private static CustomerService customerService;
            
    private final static String DB_NAME = "bookstore";
    
    protected static EntityManagerFactory emf;
    protected static EntityManager em;
      
    @BeforeClass
    public static void lookupService() throws NamingException {
        customerService = (CustomerService) new InitialContext().lookup(CUSTOMER_SERVICE_NAME);
        
        emf = Persistence.createEntityManagerFactory(DB_NAME);
        em = emf.createEntityManager();
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        
        if (em != null) {
            em.close();
        }
        if (emf != null) {
            emf.close();
        }
    }
    
    @Before
    public void cleanUp() {
        
        em.getTransaction().begin();
        
        int deletedFromSalesOrderItemEntity = em.createQuery("DELETE FROM SalesOrderItem").executeUpdate();
        int deletedFromSalesOrder = em.createQuery("DELETE FROM SalesOrder").executeUpdate();
        int deletedFromBook = em.createQuery("DELETE FROM Book").executeUpdate();
        int deletedFromCustomer = em.createQuery("DELETE FROM Customer").executeUpdate();
        int deletedFromUser = em.createQuery("DELETE FROM Login").executeUpdate();
        
        em.getTransaction().commit();
       
        StringBuilder sb = new StringBuilder();
        
        sb.append("Deleted from:\n");
        sb.append("BookEntity: " + deletedFromBook + "\n");
        sb.append("CustomerEntity: " + deletedFromCustomer + "\n");
        sb.append("SalesOrder: " + deletedFromSalesOrder + "\n");
        sb.append("SalesOrderItemEntity: " + deletedFromSalesOrderItemEntity + "\n");
        sb.append("UserEntity: " + deletedFromUser + "\n");
        
        System.out.println(sb);  
    }
    
    @Test
    public void registerCustomerPositiv() throws CustomerAlreadyExistsException, CustomerNotFoundException {
        // arrange 
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        // act
        Long number = customerService.registerCustomer(registration);
        
        // assert
        Customer result = customerService.findCustomer(number);
        
        assertEquals(EMAIL, result.getEmail());
        
    }
    
    @Test(expected = CustomerAlreadyExistsException.class )
    public void registerCustomerWithExistEMail() throws CustomerAlreadyExistsException, CustomerNotFoundException {
        // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        customerService.registerCustomer(registration);
        
        // asser
        Assert.fail();
    }
    
    @Test(expected = CustomerAlreadyExistsException.class )
    public void registerCustomerWithExistEMailCasesensitive() throws CustomerAlreadyExistsException, CustomerNotFoundException {
        // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        Registration registration2 =  createRegistration(PASSWORD, EMAIL.toUpperCase(), FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration2);
        
        // asser
        Assert.fail();
    }
    
    
    @Test
    public void findCustomerWithEmailPositiv() throws CustomerAlreadyExistsException, CustomerNotFoundException{
        
        // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        Customer result = customerService.findCustomer(EMAIL);
        
        // assert
         assertEquals(EMAIL, result.getEmail());
        
    }
    
    @Test
    public void findCustomerWithEmailPositivCasesensitive() throws CustomerAlreadyExistsException, CustomerNotFoundException{
        
        // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        Customer result = customerService.findCustomer(EMAIL.toUpperCase());
        
        // assert
         assertEquals(EMAIL, result.getEmail());
        
    }
    
    @Test(expected = CustomerNotFoundException.class)
    public void findCustomerWithEmailNegativ() throws CustomerAlreadyExistsException, CustomerNotFoundException{
        // arrange
        Registration registration = createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        customerService.findCustomer("gibtesnicht@test.ch");
        
        // assert
        Assert.fail();
    }
    
    @Test
    public void findCustomerWithCustomerNrPositiv() throws CustomerAlreadyExistsException, CustomerNotFoundException{
        // arragne 
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        Long number = customerService.registerCustomer(registration);
        
        // act
        Customer result = customerService.findCustomer(number);
        
        // assert
        assertEquals(EMAIL, result.getEmail());   
    }
    
    
    @Test(expected = CustomerNotFoundException.class)
    public void findCustomerWithCustomerNrNegativ() throws CustomerAlreadyExistsException, CustomerNotFoundException{
        // arragne 
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        Long number = customerService.registerCustomer(registration);
        number ++;
        
        // act
        customerService.findCustomer(number);
        
        // assert
        Assert.fail();
    }
    
    @Test
    public void searchCustomersPositiv() throws CustomerAlreadyExistsException {
        // arragne 
        Registration registration1 =  createRegistration("passwort", "vorname.nachname@test.ch", "Franz", "Stauffer");
        Registration registration2 =  createRegistration("pw", "test@test.ch", "Fritz", "Knauffer");
        customerService.registerCustomer(registration1);
        customerService.registerCustomer(registration2);
        
        // act
        List<CustomerInfo> result = customerService.searchCustomers("Fritz");
        
        // assert
        assertEquals(1, result.size());  
    }
    
    @Test
    public void searchCustomersPositivTeilname() throws CustomerAlreadyExistsException {
        // arragne 
        Registration registration1 =  createRegistration("passwort", "vorname.nachname@test.ch", "Franz", "Stauffer");
        Registration registration2 =  createRegistration("pw", "test@test.ch", "Fritz", "Knauffer");
        customerService.registerCustomer(registration1);
        customerService.registerCustomer(registration2);
        
        // act
        List<CustomerInfo> result = customerService.searchCustomers("Fr");
        
        // assert
        assertEquals(2, result.size());  
    }
    
    @Test
    public void searchCustomersPositivTeilname2() throws CustomerAlreadyExistsException {
        // arragne 
        Registration registration1 =  createRegistration("passwort", "vorname.nachname@test.ch", "Kurt", "Felix");
        Registration registration2 =  createRegistration("pw", "test@test.ch", "Felix", "Müller");
        Registration registration3 =  createRegistration("pw", "tester@test.ch", "Fritz", "Knauffer");
        customerService.registerCustomer(registration1);
        customerService.registerCustomer(registration2);
        customerService.registerCustomer(registration3);
        
        // act
        List<CustomerInfo> result = customerService.searchCustomers("fel");
        
        // assert
        assertEquals(2, result.size());  
    }
    
    @Test
    public void searchCustomersPositivTeilnameCasesensitive() throws CustomerAlreadyExistsException {
        // arragne 
        Registration registration1 =  createRegistration("passwort", "vorname.nachname@test.ch", "Franz", "Stauffer");
        Registration registration2 =  createRegistration("pw", "test@test.ch", "Fritz", "Knauffer");
        customerService.registerCustomer(registration1);
        customerService.registerCustomer(registration2);
        
        // act
        List<CustomerInfo> result = customerService.searchCustomers("fr");
        
        // assert
        assertEquals(2, result.size());  
    }
    
    @Test
    public void searchCustomersNegativ() throws CustomerAlreadyExistsException {
         // arragne 
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        List<CustomerInfo> result = customerService.searchCustomers("GibtEsNicht");
        
        // assert
        assertEquals(0, result.size());  
    }
    
    @Test
    public void updateCustomerPositiv() throws CustomerAlreadyExistsException, CustomerNotFoundException {
        // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        Long customerNr = customerService.registerCustomer(registration);
        Customer customer = customerService.findCustomer(customerNr);
        
        assertEquals(FIRST_NAME, customer.getFirstName());
        assertEquals(LAST_NAME, customer.getLastName());
        assertEquals("Plasselb", customer.getAddress().getCity());
        assertEquals("1737", customer.getAddress().getPostalCode());
        assertEquals("Schweiz", customer.getAddress().getCountry());
        assertEquals("Dorfstrasse 8", customer.getAddress().getStreet());
        assertEquals(new Integer("10"), customer.getCreditCard().getExpirationMonth());
        assertEquals(new Integer("2017"), customer.getCreditCard().getExpirationYear());
        assertEquals("NUMMER-1234", customer.getCreditCard().getNumber());
        assertEquals(CreditCardType.VISA, customer.getCreditCard().getType());
        // act
        customer.setFirstName("Hans");
        customer.setLastName("Müller");
        customer.getAddress().setCity("Frankfurt");
        customer.getAddress().setPostalCode("50000");
        customer.getAddress().setCountry("Deutschland");
        customer.getAddress().setStreet("Hauptstrasse 300");
        customer.getCreditCard().setExpirationMonth(new Integer("12"));
        customer.getCreditCard().setExpirationYear(new Integer("2020"));
        customer.getCreditCard().setNumber("NUMMER-8888");
        customer.getCreditCard().setType(CreditCardType.MASTERCARD);
        customerService.updateCustomer(customer);
        
        // assert
        Customer result = customerService.findCustomer(customerNr);
        assertEquals("Hans", result.getFirstName());
        assertEquals("Müller", result.getLastName());
        assertEquals("Frankfurt", result.getAddress().getCity());
        assertEquals("50000", result.getAddress().getPostalCode());
        assertEquals("Deutschland", result.getAddress().getCountry());
        assertEquals("Hauptstrasse 300", result.getAddress().getStreet());
        assertEquals(new Integer("12"), result.getCreditCard().getExpirationMonth());
        assertEquals(new Integer("2020"), result.getCreditCard().getExpirationYear());
        assertEquals("NUMMER-8888", result.getCreditCard().getNumber());
        assertEquals(CreditCardType.MASTERCARD, result.getCreditCard().getType());
    }
    
    @Test (expected = CustomerNotFoundException.class)
    public void updateCustomerNotFound() throws CustomerAlreadyExistsException, CustomerNotFoundException {
         // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        Long customerNr = customerService.registerCustomer(registration);
        Customer customer = customerService.findCustomer(customerNr);
        
        // act   
        customer.setNumber(0L);
        customerService.updateCustomer(customer);
        
        // assert
        Assert.fail();
    }
    
    @Test (expected = CustomerNotFoundException.class)
    public void updateCustomerEmail() throws CustomerAlreadyExistsException, CustomerNotFoundException {
         // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        Long customerNr = customerService.registerCustomer(registration);
        Customer customer = customerService.findCustomer(customerNr);
        String neueEmail = "neueEmail@test.ch";
        
        // act   
        customer.setEmail(neueEmail);
        customerService.updateCustomer(customer);
        
        // assert
        Customer newCustomer = customerService.findCustomer(customerNr);
        Assert.assertEquals(customerNr, newCustomer.getNumber());
        Assert.assertEquals(neueEmail.toLowerCase(), newCustomer.getEmail());
        customerService.findCustomer(EMAIL); // es gibt keinen Customer mehr mit dieser Email
    }
    
    @Test (expected = CustomerAlreadyExistsException.class)
    public void updateCustomerWithSameEmailAlreadyExists() throws CustomerAlreadyExistsException, CustomerNotFoundException {
        // arrange
       Registration registration1 =  createRegistration("passwort", "vorname.nachname@test.ch", "Franz", "Stauffer");
       Registration registration2 =  createRegistration("pw", "test@test.ch", "Fritz", "Knauffer");
       customerService.registerCustomer(registration1);
       Long customerNr2 = customerService.registerCustomer(registration2);
       Customer customer2 = customerService.findCustomer(customerNr2);
        
        // act
        customer2.setEmail("Vorname.Nachname@test.ch");
        customerService.updateCustomer(customer2);
        
        // assert
        Assert.fail();
    }
    
    @Test
    public void emailErneutVerwenden() throws CustomerAlreadyExistsException, CustomerNotFoundException {
        // arrange
        String email1 = "email1@test.ch";
        String email2 = "email2@test.ch";
        Registration registration1 =  createRegistration("passwort", email1, "Franz", "Stauffer");
        Long no = customerService.registerCustomer(registration1);
        Customer customer = customerService.findCustomer(no);
        customer.setEmail(email2);
        customerService.updateCustomer(customer);
        
        // act
        Registration registration2 =  createRegistration("pw", email1, "Fritz", "Knauffer");
        Long customerNr2 = customerService.registerCustomer(registration2);
        Customer customer2 = customerService.findCustomer(customerNr2);
        
        // assert
        Assert.assertEquals(email1, customer2.getEmail());
    }
    
    
    @Test
    public void changePasswordPositiv() throws CustomerAlreadyExistsException, CustomerNotFoundException, InvalidPasswordException {
    
        // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        customerService.changePassword(EMAIL, "neuesPasswort");
        
        // assert
        Customer customer = customerService.findCustomer(EMAIL);
        customerService.authenticateCustomer(EMAIL, "neuesPasswort");
        assertTrue(true);
        
    }
    
    @Test
    public void changePasswordPositivCasesensitive() throws CustomerAlreadyExistsException, CustomerNotFoundException, InvalidPasswordException {
    
        // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        customerService.changePassword(EMAIL.toUpperCase(), "neuesPasswort");
        
        // assert
        Customer customer = customerService.findCustomer(EMAIL);
        customerService.authenticateCustomer(EMAIL, "neuesPasswort");
        assertTrue(true);
        
    }
    
    @Test (expected = CustomerNotFoundException.class)
    public void changePasswordNegativ() throws CustomerAlreadyExistsException, CustomerNotFoundException {
        // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        customerService.changePassword("email_gibtesnicht", "neuesPasswort");
        
        // assert
        Assert.fail();
    }
    
    @Test
    public void authenticateCustomerPositiv() throws CustomerNotFoundException, CustomerAlreadyExistsException, InvalidPasswordException {
        // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        customerService.authenticateCustomer(EMAIL, PASSWORD);
        
        // assert
        assertTrue(true);
    }
    
    @Test
    public void authenticateCustomerPositivCasesensitive() throws CustomerNotFoundException, CustomerAlreadyExistsException, InvalidPasswordException {
        // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        customerService.authenticateCustomer(EMAIL.toUpperCase(), PASSWORD);
        
        // assert
        assertTrue(true);
    }
    
    @Test (expected = CustomerNotFoundException.class)
    public void authenticateCustomerCustomerNotFound() throws CustomerAlreadyExistsException, CustomerNotFoundException, InvalidPasswordException {
        // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        customerService.authenticateCustomer("email_gibtesnicht", PASSWORD);
        
        // assert
        Assert.fail();
    }
    
    @Test (expected = InvalidPasswordException.class)
    public void authenticateCustomerInvalidPassword() throws CustomerAlreadyExistsException, CustomerNotFoundException, InvalidPasswordException {
        // arrange
        Registration registration =  createRegistration(PASSWORD, EMAIL, FIRST_NAME, LAST_NAME);
        customerService.registerCustomer(registration);
        
        // act
        customerService.authenticateCustomer(EMAIL, "falschesPW");
        
        // assert
        Assert.fail();
    }
}
