1) Wahl des Collection-Types
----------------------------
Bei der Wahl des Collection-Types gingen wir davon aus, dass wir grundsätzlich ein Set verwenden, wo aber eine Ordnung sinnvoll scheint, haben wir List verwendet:
- User <-> Group: Set
- SalesOrder -> Customer: List
- SalesOrder -> SalesOrderItem: List

2) Abfrage Methoden
-------------------
Wir versuchten möglichst alle Variante die JPA bietet zu verwenden. 
- Wo mit einem Primärschlüssel gesucht wir, verwenden wir em.find.
- Constructor-Expression wo möglich und sinnvoll (dynamische Queries, Reduktion der Anzahl DB Zugriffe)

** Book **
a) a book with a particular ISBN number
    --> getBookByISBN     return BookEntity   (umgesetzt mit em.find)
    --> getBookInfoByISBN       return BookInfo     (umgesetzt mit constructorExpression)
b) all the books that contain every keyword in the title, authors or publisher field
    --> searchBooksWithContainKeywords  return List<BookEntity>     (umgesetzt mit Criteria API); BookEntities da authors und publisher gefordert sind
    Besonders: Wildcard muss gesetzt sein, damit es richtig funktioniert 
** Customer **
c) information on all the customers whose first or last name contains a particular name
    --> searchCustomerInfoByFirstOrLastName return List<CustomerInfoDTO>    (umgesetzt mit constructorExpression , da alle benötigten Informationen vorhanden sind)
** Order **
d) an order with a particular number
	--> getSalesOrderByOrderNumber	return SalesOrderEntity	(umgesetzt mit em.find)
	--> getSalesOrderByOrderNumberWithEntityGraph	return SalesOrderEntity	(umgesetzt mit em.find); Um ein Nachladen der Lazy Collections zu verhindern wird ein EntityGraph verwendet
   Die gleichen Methoden haben wir auch noch mit JPQL umgesetzt
	--> getSalesOrderByOrderNumberWithJPQL
	--> getSalesOrderByOrderNumberWithJPQLAndEntityGraph
e) information on all orders of a particular customer and a particular year
	--> searchAllSalesOrdersByCustomerAndYear	return List<OrderInfoDTO>	(umgesetzt mit constructorExpression)
f) sum of amount and number of positions and average amount per position on all orders of all customers of a particular year group by customer
	--> searchAllSalesOrderForAllCustomersForOneYear	return List<OrderStatistic>	(umgesetzt mit constructorExpression)
** User **
g) a user with a particular username
    --> getUserByName return UserEntity   (umgesetzt mit em.find)

3) Spezielle Klassen
--------------------
a) CreditCardType: Da die Bezahlung nicht vom bookstore gemacht wird sondern durch einen Payment-Service-Provider, interessiert und der Kreditkartentyp nur am Rande, deswegen verwenden wir einen String und lassen alle Werte zu.
b) OrderStatus: Wir haben für diese Klasse eine Enum gewählt, weil ein Ändern der Status auch zum Ändern der Businesscodes führt.
c) Bookbinding: Dieser Typ wird nicht von uns bestimmt, es können laufend neue Typen dazu kommen. Da wir momentan keinen Grund sehen, die Werte einzuschränken, verwenden wir auch hier einen String. Sollte sich herausstellen, dass die Werte vordefiniert werden sollen, würden wir das über eine Tabelle machen, damit ein neuer Wert keinen Code-Change zur Folge hat.

4) Interne Abmachung
--------------------
prefix  get     -> wir erwaren das Objekt, wenn nicht vorhanden, gibt es eine Exception
prefis  search  -> Objekt muss nicht vorhanden sein --> leeres Resultat möglich  (leere liste oder null)
