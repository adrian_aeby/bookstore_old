1) Wird die Email geändert, erhält der Customer ein neues Login (Email ist der Primärschlüssel von Login). Das alte Login wird gelöscht und der Benutzername (Email), kann wiederverwendet werden.

2) Theoretisch kann der lokalte Teil der Email-Adresse Casesensitive sein. Da uns aber nicht bekannt ist, ob dies überhaupte ein Anbieter unterstützt, haben wir uns entschieden die Email-Adresse immer klein geschrieben zu speichern, so dass a@bfh.ch == A.bfh.ch.

3) Folgende Variablen können im Deployment-Descriptor konfiguriert werden:
	- waitTime:	Zeit die vergehen soll bevor eine Bestellung von PROCESSING nach SHIPPED wechselt.
	- mail.to:	Empfängeradresse aller versendeten Emails.
	
4) Statusübergänge: Es werden folgende Statusübergänge automatisch durchgeführt.
	- PROCESSING kann nur erreicht werden wenn der Status ACCEPTED ist.
	- SHIPPED kann nur erreicht werden wenn der Status PROCESSING ist. 
	
5) Die Klasse org.books.application.interceptor.LogInterceptor wird als Default Interceptor verwendet und loggt alle Methoden Ein- und Ausgänge. 

6) Die ISBN wird bei uns immer gross gespeichert um möglichen Problemen mit gleichen ISBN ('ISBN' == 'isbn') vorzubeugen.

7) Die Keywörter bei der Suche nach Büchern können durch Komma oder Leerzeichen getrennt werden. Eine leere Eingabe gibt keine Bücher zurück.

8) Da im BookInfo der Preis eines Buches nicht mit dem aus dem Katalog übereinstimmen muss, und es auch keine definierte Exception dafür gibt, gehen wir davon aus, das dieser Preis immer stimmt und prüfen diesen nicht.

9) Wir haben bewusst darauf verzichtet Methoden Thread-Safe zu implementieren, da dies Mehraufwand und Performanceeinbussen zur Folge hätten, der Nutzen jedoch gering ist. So wäre es theoretisch möglich, dass zwei Bücher mit gleicher ISBN gleichzeit erfasst werden oder sich zwei Benutzer mit derselben Email registrieren. Dies kann jedoch praktisch ausgeschlossen werden.